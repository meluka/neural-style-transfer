import torch
import vgg
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

import copy
from PIL import Image
import prepare_image as pimg
import numpy as np
import matplotlib.pyplot as plt
from collections import namedtuple
from transformer_net import TransformerNet

from utils import *
import argparse
import time

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def gram_matrix(input):
    a, b, c, d = input.size() # a=batch size(=1)
    # b=number of feature maps
    # (c,d) =dimensions of a f. map (N=c*d)
    features = input.view(a * b, c * d) # resize F_XL into \hat F_XL
    G = torch.mm(features, features.t()) ## compute gram product
    # we 'normalize' the values of the gram matrix
    # by dividing by the number of element in each feature maps.
    return G.div(a * b * c * d)

## Required layers
LossOutput = namedtuple("LossOutput", ["relu1_2", "relu2_2", "relu3_3", "relu4_3"])
class vgg_feature(nn.Module):
    def __init__(self, vgg_model):
        super(vgg_feature, self).__init__()
        self.layer_name_mapping = {
            '3': "relu1_2",
            '8': "relu2_2",
            '15': "relu3_3",
            '22': "relu4_3"
        }
        ## Construct the layers
        self.vgg_copy = copy.deepcopy(vgg_model)
        
    def forward(self, x):
        #x = self.vgg_feature_model(x)
        output = {}
        i = 0
        for module in self.vgg_copy.children():#_modules.items():
            x = module(x)
            if str(i) in self.layer_name_mapping:
                output[self.layer_name_mapping[str(i)]] = x
            i += 1
        return LossOutput(**output)

## Train the network
## Training hyperparams
CONTENT_WEIGHT = 1
STYLE_WEIGHT = 1e5
LOG_INTERVAL = 10
REGULARIZATION = 1e-7
## create the model
transformer_model = TransformerNet().to(device)
## make the optimizer
optimizer = optim.SGD(transformer_model.parameters(), lr=0.001)
transformer_model.train()
def train(num_epoch, batch_size, num_batches, train_tensor, crop_size):
    for epoch in range(num_epoch):
        print('Epoch: {}'.format(epoch+1))
        print('')
        agg_style_loss = 0
        agg_content_loss = 0
        agg_total_loss = 0
        ## Load the style image
        style_img = image_loader('images/picasso.jpg', device_id=device, im_size=crop_size, \
                                    resize_img=True)
        #print(style_img.size())
        ## Get the feature part of the vgg model
        vgg19_feature = vgg.vgg19(pretrained=True).features
        feature_model = vgg_feature(vgg19_feature).to(device).eval()
        #print(feature_model)
        style_features = feature_model(style_img)
        gram_style = [Variable(gram_matrix(y), requires_grad=False) for y in style_features]
        ## Print the mean of the style gram matrix
        #print(np.mean(gram_style[3].data.cpu().numpy()))
        for i in range(num_batches):
            train_tensor_batch = train_tensor[i*batch_size:(i+1)*batch_size]
            #print(train_tensor_batch.size())
            agg_content_loss_batch = 0
            agg_style_loss_batch = 0
            agg_total_loss_batch = 0
            count = 1
            #n_batch = len()
            count += i
            for img_idx in range(batch_size):
                x = train_tensor_batch[img_idx].unsqueeze(0).to(device, torch.float)
                #print(count)
                #print(i)
                ## Set the gradient to zero
                optimizer.zero_grad()
                ## get the transformer output
                y = transformer_model(x) 
                ## Get the output image features
                y_feature = feature_model(y)
                ## Get the input image features
                x_features = feature_model(x)
                f_xc_c = Variable(x_features[1], requires_grad=False)
                ## Calculate the content loss from the second defined layer in LossOutput
                c_loss = CONTENT_WEIGHT * F.mse_loss(y_feature[1], f_xc_c)
                ## Calculate feature loss
                s_loss = 0
                reg_loss = REGULARIZATION * (
                                torch.sum(torch.abs(y[:, :, :, :-1] - y[:, :, :, 1:])) + 
                                torch.sum(torch.abs(y[:, :, :-1, :] - y[:, :, 1:, :])))

                for m in range(len(y_feature)):
                    gram_s = gram_style[m]
                    #print(gram_s.size())
                    gram_y = gram_matrix(y_feature[m])
                    #print(gram_y.size())
                    s_loss += STYLE_WEIGHT * F.mse_loss(gram_y, gram_s)
                ## Compute the total loss
                loss = s_loss + c_loss + reg_loss
                #loss = loss.detach()
                ## Backpropagate the loss
                loss.backward()
                #print(loss.item())
                ## Update the transformer network weights
                optimizer.step()	
                ## aggregate the loss
                agg_style_loss_batch += s_loss
                agg_content_loss_batch += c_loss
                agg_total_loss_batch += loss.item()
            ## print the losses
            #if (i + 1) % LOG_INTERVAL == 0:
            mseg = '{}, {}, content: {:.6f}, style: {:.6f}, total: {:.6f}'.format(
            time.ctime(), count,
            agg_content_loss_batch / batch_size, agg_style_loss_batch / batch_size,
            agg_total_loss_batch / batch_size)
            print(mseg)
            agg_content_loss += agg_content_loss_batch
            agg_style_loss += agg_style_loss_batch
            agg_total_loss += agg_total_loss_batch
        print('Epoch: {}, content: {:.6f}, style: {:.6f}, total:{:.6f}'.format(
            epoch+1, agg_content_loss / num_batches, agg_style_loss / num_batches, 
            agg_total_loss / num_batches
        ))
    
    return transformer_model.state_dict()

def test(model_name_path):
    transformer_model.eval()
    model_param = torch.load(model_name_path)
    transformer_model.load_state_dict(model_param)
    content_img = image_loader('./images/gang.jpg', device_id=device)
    output = transformer_model(content_img)
    output = output.detach()
    ## clamp the output
    output.data.clamp_(0, 1)
    plt.figure()
    imshow(output)
    plt.show()
    output = output.squeeze(0)
    #print(output.size())
    output_np = output.cpu().numpy().transpose(1, 2, 0)
    #print(output_np.shape)
    output_np = output_np * 255.0
    img = Image.fromarray(output_np.astype('uint8')).convert('RGB')
    img.save('output_img.png')

## main function definition
def main():
    print('hi')
    parser = argparse.ArgumentParser(description='parse model arguments')
    parser.add_argument('--src_train_original', dest='src_train_original', \
        default='E:/DnCNN/git.d/sknkwrk/nst/data/train_data/train2017', \
        help='folder with original training images')
    parser.add_argument('--src_save_crop', dest='src_save_crop', \
        default='E:/DnCNN/git.d/sknkwrk/nst/data/cropped_images', \
        help='folder to save the cropped images')
    parser.add_argument('--crop_size', dest='crop_size', type=int, \
        default=256, help='crop size of the image')
    parser.add_argument('--num_epoch', dest='num_epoch', type=int, \
        default=5, help='number of training epochs')
    parser.add_argument('--batch_size', dest='batch_size', type=int, \
        default=10, help='The input batch size')
    parser.add_argument('--num_batches', dest='num_batches', type=int, \
        default=4990, help='number of batchs')
    args = parser.parse_args()
    ## Crop the images and save them
    #crop_image(args.src_train_original, args.src_save_crop, args.crop_size)
    ## Get the torch image tensor for training
    train_tensor = image_to_tensor(args.src_save_crop, 3, args.crop_size, \
                                    args.crop_size, device)
    print('done')
    print(train_tensor.size())
    ## Train the network        
    model_param = train(args.num_epoch, args.batch_size, args.num_batches, train_tensor, args.crop_size)
    model_name_path = './model_dict/fast_nst_v1.pt'
    torch.save(model_param, model_name_path)
    print('model saved at: {}'.format(model_name_path))
    test(model_name_path)


if __name__ == '__main__':
    main()

			
		
		
