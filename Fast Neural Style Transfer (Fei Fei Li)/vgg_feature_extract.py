import torch
import vgg
import torch.nn as nn
import torch.nn.functional as F
import copy
from PIL import Image
import prepare_image as pimg
import numpy as np
import matplotlib.pyplot as plt
from collections import namedtuple

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def gram_matrix(input):
    a, b, c, d = input.size() # a=batch size(=1)
    # b=number of feature maps
    # (c,d) =dimensions of a f. map (N=c*d)
    features = input.view(a * b, c * d) # resize F_XL into \hat F_XL
    G = torch.mm(features, features.t()) ## compute gram product
    # we 'normalize' the values of the gram matrix
    # by dividing by the number of element in each feature maps.
    return G.div(a * b * c * d)

## Required layers
LossOutput = namedtuple("LossOutput", ["relu1_2", "relu2_2", "relu3_3", "relu4_3"])
class vgg_feature(nn.Module):
    def __init__(self, vgg_model):
        super(vgg_feature, self).__init__()
        self.layer_name_mapping = {
            '3': "relu1_2",
            '8': "relu2_2",
            '15': "relu3_3",
            '22': "relu4_3"
        }
        ## Construct the layers
        self.vgg_copy = copy.deepcopy(vgg_model)
        
    def forward(self, x):
        #x = self.vgg_feature_model(x)
        output = {}
        i = 0
        for module in self.vgg_copy.children():#_modules.items():
            x = module(x)
            if str(i) in self.layer_name_mapping:
                output[self.layer_name_mapping[str(i)]] = x
            i += 1
        return LossOutput(**output)

def image_loader(image_name, img_type='RGB'):
    image = Image.open(image_name) ## Open the image
    #image = pimg.resize_img(image, imsize_tuple)
    image_array = np.array(image)#.transpose(2, 0, 1) ## Make it an np array
    print(image_array.shape)
    if img_type == 'gray':
        image_array = np.reshape(image_array, [imsize, imsize, 1])
    image_array = image_array / 255.0
    image_array = image_array.transpose(2, 0, 1)
    image_torch = pimg.np_to_torch(image_array) ## Conver it to tensor
    image_torch = image_torch.unsqueeze(0) ## Make a fake batch dimension
    return image_torch.to(device, torch.float)

style_img = image_loader('images/bright.jpg')
input_img = image_loader('images/abba_4.jpg')
## Time to view the resized images
def imshow(tensor, title=None, img_type='RGB'):
    image = tensor.cpu().clone() ## Clone the tensor in the cpu
    image = image.squeeze(0) ## Remove the fake batch dimension
    image = image.numpy().transpose(1, 2, 0) ## Convert the tensor to numpy array
    print(np.max(image))
    #image = image / 255.0
    if img_type == 'gray':
        plt.imshow(image[:, :, 200], cmap=plt.cm.gray)
    if img_type == 'RGB':
        plt.imshow(image)
    if title is not None:
        plt.title(title)
    plt.pause(0.010) ## pause a bit to update the plots

vgg19_feature = vgg.vgg19(pretrained=True).features
feature_model = vgg_feature(vgg19_feature).to(device).eval()
#print(feature_model)
output_img = feature_model(style_img)
gram_style = [gram_matrix(y) for y in output_img]
print(gram_style[0])
'''
output_img = output_img.detach()
plt.figure()
imshow(input_img)
plt.figure()
imshow(output_img, img_type='gray')
plt.show()
'''