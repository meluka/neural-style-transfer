from PIL import Image
import prepare_image as pimg
import glob
import os
import PIL
import matplotlib.pyplot as plt

import numpy as np
import torch

def image_loader(image_name, device_id, im_size=256, resize_img=False, img_type='RGB'):
    image = Image.open(image_name) ## Open the image
    if resize_img:
        new_size = [im_size, im_size]
        image = image.resize(new_size, resample=PIL.Image.BICUBIC)
    #image = pimg.resize_img(image, imsize_tuple)
    image_array = np.array(image)#.transpose(2, 0, 1) ## Make it an np array
    #print(image_array.shape)
    if img_type == 'gray':
        image_array = np.reshape(image_array, [imsize, imsize, 1])
    image_array = image_array / 255.0
    image_array = image_array.transpose(2, 0, 1)
    image_torch = pimg.np_to_torch(image_array) ## Conver it to tensor
    image_torch = image_torch.unsqueeze(0) ## Make a fake batch dimension
    return image_torch.to(device_id, torch.float)
    
## Time to view the resized images
def imshow(tensor, title=None, img_type='RGB'):
    image = tensor.cpu().clone() ## Clone the tensor in the cpu
    image = image.squeeze(0) ## Remove the fake batch dimension
    image = image.numpy().transpose(1, 2, 0) ## Convert the tensor to numpy array
    print(np.max(image))
    #image = image / 255.0
    if img_type == 'gray':
        plt.imshow(image[:, :, 200], cmap=plt.cm.gray)
    if img_type == 'RGB':
        plt.imshow(image)
    if title is not None:
        plt.title(title)
    plt.pause(0.010) ## pause a bit to update the plots
    
## Crop all the images in a given size (center crop)
def crop_image(load_path, save_path, crop_size):
    ## Load the image paths to a list
    file_list = glob.glob(load_path + '/*.jpg')
    name = 'train_img_'
    ## Crop the centers
    for i in range(len(file_list)):
        file = file_list[i]
        pil_img = Image.open(file).convert('RGB')
        ## get image centers
        im_h_half, im_w_half = (pil_img.size[0] / 2), (pil_img.size[1] / 2)
        ## Crop the image
        img_cropped = pil_img.crop(
                                    (im_w_half - (crop_size / 2),
                                    im_h_half - (crop_size / 2),
                                    im_w_half + (crop_size / 2),
                                    im_h_half + (crop_size / 2)))
        file_name = name + str(i) + '.png'
        img_cropped.save(os.path.join(save_path, file_name))
        if i == 100000:
            break

## Load cropped images and convert them to 4D torch tensors
def image_to_tensor(load_path, channels, im_h, im_w, device_id):
    ## Load the image paths to a list
    file_list = glob.glob(load_path + '/*.png')[0:50000]
    #print(file_list)
    ## 4D zero array to store the images
    np_img4D = np.zeros((len(file_list), channels, im_h, im_w))
    ## Convert the images to a 4D tensor of (batch, num_channels, height, width)
    for i in range(len(file_list)):
        file = file_list[i]
        pil_img = Image.open(file).convert('RGB')
        ## Convert the pil object to numpy array
        np_img = np.array(pil_img) / 255.0
        ## Transpose the array for corresponding dim pattern
        np_img = np_img.transpose(2, 0, 1)
        ## Store image in 4D numpy array
        np_img4D[i] = np_img
    #print(np.max(np_img4D))
    ## Convert the 4D numpy array to 4D torch tensor
    torch_img4D = torch.from_numpy(np_img4D)
    return torch_img4D
                
    
        
    