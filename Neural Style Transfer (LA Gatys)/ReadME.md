This code contains the neural style transfer model introduced by LA Gatys.
The code is written as a part of pytorch tutorials, link below.
https://pytorch.org/tutorials/advanced/neural_style_tutorial.html