from __future__ import print_function
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import vgg

from PIL import Image
import matplotlib.pyplot as plt
import prepare_image as pimg

import copy
import numpy as np
## Check the available devices to move the tensors
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
#device = 'cpu'
## Desired size of the output image
imsize = 960 if torch.cuda.is_available() else 128 ## use small img if no gpu
## Make tuple of image size
imsize_tuple = [720, 960]
## move images to devices
def image_loader(image_name, img_type='RGB'):
    image = Image.open(image_name) ## Open the image
    image = pimg.resize_img(image, imsize_tuple)
    image_array = np.array(image)#.transpose(2, 0, 1) ## Make it an np array
    print(image_array.shape)
    if img_type == 'gray':
        image_array = np.reshape(image_array, [imsize, imsize, 1])
    image_array = image_array / 255.0
    image_array = image_array.transpose(2, 0, 1)
    image_torch = pimg.np_to_torch(image_array) ## Conver it to tensor
    image_torch = image_torch.unsqueeze(0) ## Make a fake batch dimension
    return image_torch.to(device, torch.float)

## Load the images, both style and content
style_img = image_loader('images/bright.jpg')
content_img = image_loader('images/abba_4.jpg')
## Check if the sizes are similar
assert style_img.size() == content_img.size(), \
    "we need to import style and content images of the same size"
## Time to view the resized images
def imshow(tensor, title=None, img_type='RGB'):
    image = tensor.cpu().clone() ## Clone the tensor in the cpu
    image = image.squeeze(0) ## Remove the fake batch dimension
    image = image.numpy().transpose(1, 2, 0) ## Convert the tensor to numpy array
    print(np.max(image))
    #image = image / 255.0
    if img_type == 'gray':
        plt.imshow(image[:, :, 0], cmap=plt.cm.gray)
    if img_type == 'RGB':
        plt.imshow(image)
    if title is not None:
        plt.title(title)
    plt.pause(0.001) ## pause a bit to update the plots

## Check the pictures
plt.figure()
imshow(style_img, title='Style image')
plt.figure()
imshow(content_img, title='Content image')
plt.show()

def gram_matrix(input):
    a, b, c, d = input.size() # a=batch size(=1)
    # b=number of feature maps
    # (c,d) =dimensions of a f. map (N=c*d)
    features = input.view(a * b, c * d) # resize F_XL into \hat F_XL
    G = torch.mm(features, features.t()) ## compute gram product
    # we 'normalize' the values of the gram matrix
    # by dividing by the number of element in each feature maps.
    return G.div(a * b * c * d)

class ContentLoss(nn.Module):
    def __init__(self, target):
        super(ContentLoss, self).__init__()
        # we 'detach' the target content from the tree used
        # to dynamically compute the gradient: this is a stated value,
        # not a variable. Otherwise the forward method of the criterion
        # will throw an error.
        self.target = target.detach()

    def forward(self, input):
        self.loss = F.mse_loss(input, self.target)
        return input

class StyleLoss(nn.Module):
    def __init__(self, target_features):
        super(StyleLoss, self).__init__()
        self.target = gram_matrix(target_features).detach() ## detach from the main graph

    def forward(self, input):
        G = gram_matrix(input)
        self.loss = F.mse_loss(G, self.target)
        return input

cnn = vgg.vgg19(pretrained=True).features.to(device).eval()

## The vgg networks are trained on images with each channel normalized by mean=[0.485, 0.456, 0.406]
## and std=[0.229, 0.224, 0.225], we will use these values to normalize the images before sending them
## to the network
cnn_normalization_mean = torch.tensor([0.485, 0.456, 0.406]).to(device)
cnn_normalization_std = torch.tensor([0.229, 0.224, 0.225]).to(device)
## create a module to normalize the image so we can put it into a nn.Sequential
class Normalization(nn.Module):
    def __init__(self, mean, std):
        super(Normalization, self).__init__()
        # .view the mean and std to make them [C x 1 x 1] so that they can
        # directly work with image Tensor of shape [B x C x H x W].
        # B is batch size. C is number of channels. H is height and W is width.
        self.mean = torch.tensor(mean).view(-1, 1, 1)
        self.std = torch.tensor(std).view(-1, 1, 1)

    def forward(self, img):
        # normalize image
        return (img - self.mean) / self.std
## In vgg19 model there exists a feature extractor and a classifier separately
## implemented using nn.Sequential
## we need content and style loss modules to be transparent layers in our network
## with a desired depth. Therefore we are going to construct a Sequential where the
## loss modules and vgg layers ordered in a proper sequence
## Desired depth of layers to compute style and content loss
content_layers_default = ['conv_3']
style_layers_default = ['conv_1', 'conv_2', 'conv_3', 'conv_4', 'conv_5', 'conv_6', 'conv_7', 'conv_8', \
                        'conv_9', 'conv_10']

def get_style_model_and_loss(cnn, normalization_mean, normalization_std,
                            style_img, content_img, content_layers=content_layers_default,
                            style_layers=style_layers_default):

    cnn = copy.deepcopy(cnn)
    ## To have iterables to content and style loss
    content_losses = []
    style_losses = []
    ## Start constructing the model
    ## normalization module
    normalization = Normalization(normalization_mean, normalization_std).to(device)
    ## Assuming cnn is Sequential, lets create a net nn.Sequential to add the modules that
    ## needed to be activated sequenctially
    model = nn.Sequential(normalization)
    i = 0 ## an incrementor
    for layer in cnn.children():
        if isinstance(layer, nn.Conv2d):
            #print(isinstance(layer, nn.Conv2d))
            i += 1
            name = 'conv_{}'.format(i)
        if isinstance(layer, nn.ReLU):
            name = 'relu_{}'.format(i)
            # The in-place version doesn't play very nicely with the ContentLoss
            # and StyleLoss we insert below. So we replace with out-of-place
            # ones here.
            layer = nn.ReLU(inplace=False)
        if isinstance(layer, nn.MaxPool2d):
            name = 'pool_{}'.format(i)
        if isinstance(layer, nn.BatchNorm2d):
            name = 'bn_{}'.format(i)
        #else:
        #    raise RuntimeError('Unrecognized layer: {}'.format(layer.__class__.__name__))
        ## Add the layers to new nn.Sequential
        model.add_module(name, layer)
        ## if a perticular layer is similar to that of content_layers, add the content module
        ## after that layer to calculate the content loss
        if name in content_layers:
            #print('content')
            ## get the target from the feature layers of vgg
            target = model(content_img).detach()
            ## Construct the content module
            content_loss = ContentLoss(target)
            ## Add the content_loss module to sequence
            model.add_module('content_loss_{}'.format(i), content_loss)
            ## append the content loss to the content_losses list
            content_losses.append(content_loss)
        ## Do the same as in content loss to the style loss
        if name in style_layers:
            #print('style')
            target_feature = model(style_img).detach()
            style_loss = StyleLoss(target_feature)
            model.add_module('style_loss_{}'.format(i), style_loss)
            style_losses.append(style_loss)
    ## Lets keep the last content and style loss modules and trim off the rest
    ## get the content model
    for i in range(len(model) - 1, -1, -1): ## Iterate in reverse
        ## Check if the layer belongs to one of the style or content loss modules
        if isinstance(model[i], ContentLoss) or isinstance(model[i], StyleLoss):
            print(model[i])
            break
    ## Trim off
    model = model[:(i+1)] ## a[:(i+1)] is as same as a[0:(i+1)]

    return model, style_losses, content_losses
## Give a white-noise input
#input_img = torch.randn(content_img.size(), device=device)
#input_img = torch.randn(style_img.size(), device=device)
input_img = content_img.clone()
## In neural style transfer, we use L-BFGS to optimize the input image. Unlike in normal
## neural network optimization, we are only optimizing the tensor of the input image. Therefor,
## make sure to do .requires_grad_()True.
def get_input_optimizer(input_img):
    optimizer = optim.LBFGS([input_img.requires_grad_()])
    return optimizer

## Last step: the loop of gradient descent. At each step, we must feed the network with the
## updated input in order to compute the new losses, we must run the backward methods of each loss
##to dynamically compute their gradients and perform the step of gradient descent. The optimizer
## requires as argument a “closure”: a function that reevaluates the model and returns the loss.
def run_style_transfer(cnn, normalization_mean, normalization_std,
                        content_img, style_img, input_img, num_steps=800,
                        style_weight=1000000, content_weight=1):
    print('building the style transfer model')
    model, style_losses, content_losses = get_style_model_and_loss(cnn, normalization_mean,
                                            normalization_std, style_img, content_img)
    optimizer = get_input_optimizer(input_img)
    print('Optimizing...')
    run = [0]
    while run[0] <= num_steps:
        def closure():
            ## The optimization could mess the input image by over optimizing it, which would
            ## lead the input image to have values between -inf to inf, therefor we must constrain
            ## the input_img to be between 0 and 1
            input_img.data.clamp_(0, 1)
            ## zero_grad the optimizer
            optimizer.zero_grad()
            ## evaluate the input image
            model(input_img)
            style_score = 0
            content_score = 0
            ## get the style loss and content loss in each layer to compute the overall loss
            for sl in style_losses:
                style_score += sl.loss
            for cl in content_losses:
                content_score += cl.loss
            ## weight the style and content loss
            style_score *= style_weight
            content_score *= content_weight
            ## compute the overall loss
            loss = style_score + content_score
            ## Do the backpropagation
            loss.backward()
            run[0] += 1
            if run[0] % 50 == 0:
                print("run {}:".format(run))
                print('Style Loss : {:4f} Content Loss: {:4f}'.format(
                    style_score.item(), content_score.item()))
                print()

            return style_score + content_score
        ## Do the optimization of the input image
        optimizer.step(closure)

    ## Final clamping of the image
    input_img.data.clamp_(0, 1)

    return input_img

## Lets do some style transfer
output = run_style_transfer(cnn, cnn_normalization_mean, cnn_normalization_std,
                            content_img, style_img, input_img)

output = output.detach()
plt.figure()
imshow(output, title='output image')
plt.ioff()
plt.show()
output = output.squeeze(0)
output_cpu = output.cpu()
output_np = output_cpu.numpy().transpose(1, 2, 0)
output_np = output_np * 255.0
output_img = Image.fromarray(output_np.astype('uint8')).convert('RGB')
output_img.save('t_abba.jpg')
